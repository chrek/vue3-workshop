const app = Vue.createApp({
  data() {
    return {
      fullName: "Chr EK",
      counter: 0,
      date_of_birth: 2000,
      message: "",
      age: 0,
      fruits: ["Banana", "Apple", "PineApple", "Avocados", "Cherry"],
    };
  },
  computed: {
    age() {
      return 2021 - this.date_of_birth;
    },
  },
  watch: {
    counter(value) {
      if (value == 60) {
        this.fullName = this.fullName + " @ " + value;
      }
    },
  },
  methods: {
    addTen() {
      this.counter = this.counter + 10;
    },
    UpdateMessage(e) {
      this.message = e.target.value;
    },
  },
});
app.mount("#app");
