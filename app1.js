const app = Vue.createApp({
  data() {
    return {
      course: "Intro to Vue with Vue 3",
      description: "This is a Vue 3 Workshop",
      fee: "100€",
      available: true,
      // imgURL:
      //   "https://images.unsplash.com/photo-1618477388954-7852f32655ec?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=80",
      imgURL: "images/photo-1618477388954-7852f32655ec.webp2.webp",
      imgDescription: "An image from unsplash.com",
      topics: [
        { id: 1, name: "Front-end" },
        { id: 2, name: "Back-end" },
        { id: 3, name: "Full-stack" },
      ],
    };
  },
  computed: {
    hasImageDescription() {
      return this.imgDescription.length > 0 ? this.imgDescription : 'This description is automated!'
    }

  },
  methods: {
    purchase() {
      console.log('You have paid for this course!')
    }
    
  },
})
app.mount('#app');