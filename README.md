# vue3-workshop

This is a Vue.js 3 project for reference purposes

## Shopping List

See the folder in fundamentals-shoppinglist

- Template Syntax and Expressions
- List Rendering
- User Inputs
- User Events
- Methods
- Conditional Rendering
- HTML Attribute Binding
- Dynamic CSS classes
- Computed Properties


## References

- [vueschool](https://vueschool.io/lessons/)

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
